This ia a 2D Tank game project.

1. Base on Unity3d
2. Similar with the FC Tank game
3. Use A* Pathfinding to move the enemy
4. Use simple State Machine to implement the enemy patrol , chase and attack AI behaviour.
5. I don't want to waste more time on this project to implement all the features of origin game. 

Last Coud Build
https://developer.cloud.unity3d.com/share/WkabJiJ0rz/

*You may need to open the WebGL flags in chrome to play*

Thanks for : 

Sebastian Lague [A* Pathfinding Tutorial](https://www.youtube.com/watch?v=-L-WgKMFuhE&list=PLFt_AvWsXl0cq5Umv3pMC9SPnKjfp9eGW) . Source [Pathfinding-2D](https://github.com/SebLague/Pathfinding-2D)

Luneyco [How to create a 2D Map Editor in Unity](https://www.youtube.com/watch?v=2j7AZdv0yr0&list=PLZvnsP8zkW0fvStNdFhF05dzoXXIOA2-R) . Source [simple-unity-2d-map-editor](https://github.com/Desnoo/simple-unity-2d-map-editor) 
The tutorial project under MIT License. The License file in the `3rd_party_licenses/Desnoo` folder.

-----

1. 基于Unity3D 引擎制作的2d Tank游戏.
2. 模仿经典红白机Tank 游戏。 
3. 通过 A* 寻路算法，实现敌人移动. 
4. 简单状态机实现敌方巡逻，追击，攻击AI。 
5. 未能全部实现原游戏玩法。 

云端构建 https://developer.cloud.unity3d.com/share/WkabJiJ0rz/

*Chrome 浏览器需要开启 WebGL 选项才能运行*

感谢 Sebastian Lague 和 Luneyco的教程