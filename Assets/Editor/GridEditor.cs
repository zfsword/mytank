using UnityEngine;
using UnityEditor;

using System.Collections;
using System.IO;

[CustomEditor(typeof(TileGrid))]
public class GridEditor : Editor{
	TileGrid grid;
	private int oldIndex = 0;

    /*void Awake(){
        CreateTileSet();
    }*/

	void OnEnable(){
        grid = (TileGrid)target;
	}

    // 添加菜单项
	[MenuItem("Assets/Create/TileSet")]
	static void CreateTileSet(){
        // 创建scrptable object Tilset
		TileSet asset = ScriptableObject.CreateInstance<TileSet>();
        // 如果用户当前选择了路径，获取选中目标的路径
		string path = AssetDatabase.GetAssetPath(Selection.activeObject);

        // 如果路径空,设为Assets
		if(string.IsNullOrEmpty(path)){
			path = "Assets";
        // 如果选择的是文件(扩展名不空)
		}else if(Path.GetExtension(path) != ""){

			// 去除文件名,变为路径
			path = path.Replace(Path.GetFileName(path), "");
        // 如过选择的是路径, 在路径后加 斜杠
		}else{
			path += "/";
		}

        // 生成唯一路径+文件名
		string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "TileSet.asset");
        // 创建Asset
		AssetDatabase.CreateAsset(asset, assetPathAndName);
		AssetDatabase.SaveAssets();
        // Brings the project window to the front and focuses it.
		EditorUtility.FocusProjectWindow();
        
		Selection.activeObject = asset;
        // The object will not be saved to the scene. It will not be destroyed when a new scene is loaded. It is a shortcut for HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor | HideFlags.DontUnloadUnusedAsset.
		// asset.hideFlags = HideFlags.DontSave;
        //asset.hideFlags = HideFlags.None;
	}

	public override void OnInspectorGUI(){
		// base.OnInspectorGUI();
		grid.width = CreateSlider("Width", grid.width);
		grid.height = CreateSlider("Height", grid.height);
		grid.distanceVertical = CreateSlider("Vertical Distance: ", grid.distanceVertical);
		grid.distanceHorizontal = CreateSlider("Horizontal Distance: ", grid.distanceHorizontal);
		grid.lineLength = CreateSlider("Line Length: ", grid.lineLength);

		if(GUILayout.Button("Open Grid Window")){
			TileGridWindow window = (TileGridWindow)EditorWindow.GetWindow(typeof(TileGridWindow));
			window.init();
		}

		//Tile Prefab
		EditorGUI.BeginChangeCheck();
		Transform newTilePrefab = EditorGUILayout.ObjectField("Tile Prefab", grid.tilePrefab, typeof(Transform), false) as Transform;
		if(EditorGUI.EndChangeCheck()){
			grid.tilePrefab = newTilePrefab;
			Undo.RecordObject(target, "Grid Changed");
		}

		// Tile Prefab parent
		EditorGUI.BeginChangeCheck();
		Transform tilePrefabParent = EditorGUILayout.ObjectField("Prefab Parent", grid.prefabParent, typeof(Transform), true) as Transform;
		if(EditorGUI.EndChangeCheck()){
			grid.prefabParent = tilePrefabParent;
			Undo.RecordObject(target, "Parent Changed");
		}
		

		//Tile Set
		EditorGUI.BeginChangeCheck();
		TileSet newTileSet = EditorGUILayout.ObjectField("TileSet", grid.tileSet, typeof(TileSet), false) as TileSet;
		if(EditorGUI.EndChangeCheck()){
			grid.tileSet = newTileSet;
			Undo.RecordObject(target, "Grid Changed");
		}


        
		if(grid.tileSet != null){
			EditorGUI.BeginChangeCheck();
			string[] names = new string[grid.tileSet.prefabs.Length];  //  获取tileSet 中所有prefab names
			int[] values = new int[names.Length];

			// 读取names，并为values赋值
			for(int i=0;i<names.Length;i++){
				names[i] = grid.tileSet.prefabs[i] != null ? grid.tileSet.prefabs[i].name : "";
				values[i] = i;
			}

            // 选择Tile 列表
			int index = EditorGUILayout.IntPopup("Select Tile", oldIndex, names, values);

			if(EditorGUI.EndChangeCheck()){
                // 记录undo目标
				Undo.RecordObject(target, "Grid Changed");
				
				// Debug.Log(index);
                // 如果index变化了
				if(oldIndex != index){
                
					oldIndex = index;  // 记录index
					grid.tilePrefab = grid.tileSet.prefabs[index];  // 更新当前tilePrefab

					float width = 0.0f;  // 当前prefab的width
					float height = 0.0f; // 当前prefab的height

					// 如果prefab 有Renderer,根据renderer计算出宽高
					SpriteRenderer renderer = grid.tilePrefab.GetComponent<SpriteRenderer>();
                    // 如果有renderer的话
					if( renderer != null){
						width = renderer.bounds.size.x;
						height = renderer.bounds.size.y;

					}else{
                        // 获取子对象的renderer
						SpriteRenderer[] childrenRenderers = grid.tilePrefab.GetComponentsInChildren<SpriteRenderer>(true);
						
						// 如果子对象有Renderer
						if(childrenRenderers.Length > 0){

							// 查找出最大和最小的边界点
							Bounds tempBound = childrenRenderers[0].bounds;
							float minX = tempBound.min.x;
							float maxX = tempBound.max.x;
							float minY = tempBound.min.y;
							float maxY = tempBound.max.y;
							for(int i=1; i < childrenRenderers.Length; i++)
							{
								// Debug.Log("children "+ i + " center "+childrenRenderers[i].bounds.center + " max " + childrenRenderers[i].bounds.max
								// 	+ " min " + childrenRenderers[i].bounds.min + " extends " + childrenRenderers[i].bounds.extents +
								// 	" size " + childrenRenderers[i].bounds.size
								// 	);

								tempBound = childrenRenderers[i].bounds;

								minX = Mathf.Min(tempBound.min.x, minX);
								minY = Mathf.Min(tempBound.min.y, minY);
								maxX = Mathf.Max(tempBound.max.x, maxX);
								maxY = Mathf.Max(tempBound.max.y, maxY);
							}
							// 最大点减去最小点得出宽高
							width = maxX - minX;
							height = maxY - minY;
						}
					}

					grid.width = width;
					grid.height = height;
				}
			}
		}
	}

	// 创建滑动条
	private float CreateSlider(string labelName, float sliderVar){
		GUILayout.BeginHorizontal();
		GUILayout.Label("Grid " + labelName);
		sliderVar = EditorGUILayout.Slider(sliderVar, 0.01f, 100.0f);
		GUILayout.EndHorizontal();

		return sliderVar;
	}

	void OnSceneGUI(){
		int controlId = GUIUtility.GetControlID(FocusType.Passive);	// 获取控件ID，并且控件 This control can never recieve keyboard focus.
		Event e = Event.current;	// 获取当前事件
		//	由于屏幕坐标系，向下方向为Y轴正方向，故用Camera.pixelHeight - mousePosition.y 得出y坐标
		Ray ray = Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x, Camera.current.pixelHeight - e.mousePosition.y));
		Vector3 mousePos = ray.origin;

		// if(e.isMouse && e.button == 0){
		// 	if(e.type == EventType.MouseDown){
        // 如果按下A键
		if(e.keyCode == KeyCode.A){
			if(e.type == EventType.KeyDown){
				GUIUtility.hotControl = controlId;
				e.Use();

				GameObject gameObject;
				Transform prefab = grid.tilePrefab;

				if(prefab){
					// Unity automatically groups undo operations by the current group index.
					Undo.IncrementCurrentGroup();

					// 创建prefab
					gameObject = PrefabUtility.InstantiatePrefab(prefab.gameObject) as GameObject;
					// 按网格对齐position
					Vector3 aligned = new Vector3(Mathf.Floor(mousePos.x/grid.width)*grid.width + grid.width*0.5f, Mathf.Floor(mousePos.y/grid.height)*grid.height + grid.height*0.5f);
					// 设置prefab位置
					gameObject.transform.position = aligned;
					gameObject.transform.parent = grid.prefabParent.transform;

					// Register an undo operations for a newly created object.
					Undo.RegisterCreatedObjectUndo(gameObject,"Create " + gameObject.name);
				}
            // 按键抬起，释放ControlId
			}else if(e.GetTypeForControl(controlId) == EventType.KeyUp){
				GUIUtility.hotControl = 0;
			}
			// }else if(e.GetTypeForControl(controlId) == EventType.MouseUp){
			// 	GUIUtility.hotControl = 0;
			// }
		}

		// if(e.isKey && e.keyCode == KeyCode.D){
		// 	RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector3.forward);
		// 	if(hit.transform != null){
		// 		GUIUtility.hotControl = controlId;
		// 		e.Use();
		// 		Undo.DestroyObjectImmediate(hit.transform.gameObject);
		// 	}else{
		// 		GUIUtility.hotControl = 0;
		// 	}
		// }
	}
}
