﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : Health{

	public override void Die(){
		base.Die();

		GameManager.Instance.PlayerCurrentLife--;
		if(GameManager.Instance.PlayerCurrentLife <= 0){
			GameManager.Instance.GameOver();
		}else{
			//TODO: reset player
			GameManager.Instance.ResetPlayerPosition();
		}
	}
}
