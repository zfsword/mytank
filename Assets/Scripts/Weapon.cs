using UnityEngine;
using System.Collections;

/// <summary>
///   武器类,用于管理,发射武器
/// </summary>
public class Weapon: MonoBehaviour {


	public GameObject projectilePrefab;
	private string projectileName;

    [Tooltip("发射相对初始位置")]
	public float spawnRelativePos = 0.20f;  /// 相对生产位置
    
	public float shootingRate = 0.25f;	// 射击速率

	private float shootCoolDown = 0.0f; // 计时器

	

	// Use this for initialization
	void Start () {
		projectileName = projectilePrefab.name;
	}
	
	// Update is called once per frame
	void Update () {
		if(shootCoolDown > 0.0f)
		{
            shootCoolDown -= Time.deltaTime;
		}
	}

	/// <summary>
	/// Attack
	/// </summary>
	/// <param name="isEnemy">是否是敌人攻击</param>
	public void Attack(bool isEnemy){
        // 如果可攻击
		if (CanAttack){
			shootCoolDown = shootingRate;	//重置计时器

			// 设置子弹发射位置
			Vector3 spawnPos = transform.position + transform.up * spawnRelativePos;
			//Transform shotTransform = Instantiate(shotPrefab, spawnPos, transform.rotation) as Transform;

			GameObject bullet;
			bullet = ObjectPooler.Instance.GetObjectByName(projectileName);
			// if(isEnemy == true){
			// 	// bullet = ObjectPooler.Instance.GetObjectByTag("ProjectileEnemy");
			// 	bullet = ObjectPooler.Instance.GetObjectByName("ProjectileEnemy");
			// }else{
			// 	bullet = ObjectPooler.Instance.GetObjectByName("ProjectilePlayer");
			// 	// bullet = ObjectPooler.Instance.GetObjectByName("ProjectileEnemy");
			// }

			bullet.SetActive(true);
			bullet.transform.position = spawnPos;
			bullet.transform.rotation = transform.rotation;

			// 设置发射方
            Projectile projectile = bullet.GetComponent<Projectile>();
            if(projectile != null){
                projectile.isEnemyShot = isEnemy;
            }else{
                Debug.Log ("Projectile 组件缺失");
            }


			Move move = bullet.GetComponent<Move>();
            if(move != null){
                move.direction = transform.up; // 设置移动方向
				move.Resume();
            }

			// 设置自销毁
			// GameManager.Instance.DelayExecute.Add(projectile.DestroySelf, projectile.destroySecends);
		}
	}

	public bool CanAttack{
		get{
			return shootCoolDown <= 0.0f;
		}
	}
}
