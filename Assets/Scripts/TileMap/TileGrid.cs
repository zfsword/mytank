using UnityEngine;
using System.Collections;

// 网格类
public class TileGrid : MonoBehaviour {

	public float distanceVertical = 10.0f;  // 从摄相机中心，到顶部或底部距离
	public float distanceHorizontal = 10.0f; // 从摄相机中心，到左边或右边距离
	public float lineLength = 10.0f;	// 线长度

	public float width = 32.0f;	// 网格宽度
	public float height = 32.0f;	// 网格高度

	public Color color = Color.white;	// 网格颜色

	public Transform tilePrefab;	// 准备绘制的tile prefab
	public Transform prefabParent;	// tile prefab 的父对象


    #if UNITY_EDITOR
    public TileSet tileSet;
    #endif
    //[SerializeField]
	//public TileSet tileSet;

    /*private Material ownedMaterial;
    void OnEnable() {
        ownedMaterial = new Material(Shader.Find("Diffuse"));
        ownedMaterial.hideFlags = HideFlags.HideAndDontSave;
        GetComponent<Renderer>().sharedMaterial = ownedMaterial;
    }
    
    // Objects created as hide and don't save must be explicitly destroyed by the owner of the object.
    void OnDisable() {
        DestroyImmediate(ownedMaterial);
    }*/


	void OnDrawGizmos(){
		Vector3 pos = Camera.current.transform.position;
		Gizmos.color = this.color;
		// 从Camera 上面 800 到 下面800循环，每次y增加 height
		for(float y = pos.y - distanceVertical; y < pos.y + distanceVertical; y+=this.height){
			Gizmos.DrawLine(new Vector3(-lineLength, Mathf.Floor(y/height)*height, 0.0f), new Vector3(lineLength, Mathf.Floor(y/height)*height, 0.0f));
		}

		for(float x = pos.x - distanceHorizontal; x < pos.x + distanceHorizontal; x+=this.width){
			Gizmos.DrawLine(new Vector3(Mathf.Floor(x/width)*width, -lineLength, 0.0f), new Vector3(Mathf.Floor(x/width)*width, lineLength, 0.0f));
		}
	}
}
