using UnityEngine;
using UnityEditor;
using System.Collections;

public class TileGridWindow : EditorWindow{
	TileGrid grid;

	public void init(){
		grid = (TileGrid)FindObjectOfType(typeof(TileGrid));
	}

	void OnGUI(){
		grid.color = EditorGUILayout.ColorField(grid.color, GUILayout.Width(200));
	}
}
