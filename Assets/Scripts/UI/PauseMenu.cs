﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : Singleton<PauseMenu>{

	[SerializeField] private GameObject pausePanel;

	void Start()
    {
        pausePanel.SetActive(false);
    }

    void Update()
    {
        if(InputController.Instance.Escape) 
        {
            if (false == pausePanel.activeInHierarchy) 
            {
                PauseGame();
            }else
            {
                ResumeGame();
            }
        } 
    }

	public void PauseGame()
    {
        Time.timeScale = 0;
        pausePanel.SetActive(true);
        //Disable scripts that still work while timescale is set to 0
    } 

    public void ResumeGame()
    {
        Time.timeScale = 1;
        pausePanel.SetActive(false);
        //enable the scripts again
    }

}
