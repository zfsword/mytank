﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController: Singleton<UIController>{

	[SerializeField]
    private float levelStartDelay = 2.0f; /// 关卡开始延时

	// [SerializeField]
	// private int[] gameMenuScenes; // 游戏菜单场景

	[SerializeField]
	private GameObject[] UiToHideInMenu; // 不想在Menu scene 显示的UI元素


	[SerializeField]
    private Text levelText;      // 管卡文本

 
    [SerializeField]
    private Image levelImage;   /// 关卡图片

	
	public override void Awake(){
		base.Awake();

		SceneManager.sceneLoaded += OnLevelFinishedLoading;
	}

	void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
		if(scene.buildIndex > LevelManager.Instance.MaxLevelIndex || scene.buildIndex < LevelManager.Instance.MinLevelIndex ){
			// 禁止游戏输入
			InputController.Instance.couldInput = false;
			// 隐藏游戏关卡信息
			SetUIElementsActive(UiToHideInMenu, false);
		}else{
			SetUIElementsActive(UiToHideInMenu, true);
			ShowLevelInfo("LEVEL " + scene.buildIndex);
		}
    }

	
	void SetUIElementsActive(GameObject[] elements, bool active){
		foreach (GameObject item in elements)
		{
			item.SetActive(active);
		}
	}

	void HideLevelImage()
    {
        levelImage.gameObject.SetActive(false);
        
        InputController.Instance.couldInput = true;
    }

    void ShowLevelInfo(string info){
        InputController.Instance.couldInput = false;
        levelText.text = info;
        levelImage.gameObject.SetActive(true);
        Invoke("HideLevelImage", levelStartDelay);
    }

}
