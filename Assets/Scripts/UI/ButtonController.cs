﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour {
	public void StartGame(){
		LevelManager.Instance.LoadFirstLevel();
	}

	public void Restart(){
		LevelManager.Instance.RestartLevel();
	}

	public void MainMenu(){
		LevelManager.Instance.LoadMainScene();
	}

	public void Quit(){
		Application.Quit ();
	}


}
