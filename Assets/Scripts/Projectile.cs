using UnityEngine;
using System;
using System.Collections;

/// <summary>
///   炮弹类, 用于炮弹相关操作
/// </summary>
public class Projectile: MonoBehaviour {

	public int damage = 2;      /// 子弹伤害
    //private int bulletHealth = 1; // 子弹血量,暂时不用

    [Tooltip("是否是敌人发出")]
	public bool isEnemyShot = false; /// 是否是敌人发出
    public float destroySecends = 10.0f; /// 自动销毁时间

    // LineCastAll 相关变量,用于检测并消除砖块
    // private Vector2 castOrigin;    // CastAll 起点
    public float castLeftShift = 0.1f;     // 起点左偏移量
    public float castUpShift = 0.06f;       // 起点上偏移量
    public Vector2 boxCastSize = new Vector2(0.06f, 0.06f);      // BoxCastAll 大小
    public float boxCastDistance = 0.18f;    // BoxCastAll 移动距离
    public LayerMask castLayer;     // CastAll layer;

    private Move move;   //  移动模块
    
    void Awake(){
        // 获取移动模块
        move = GetComponent<Move>();
    }

    void OnEnable(){
        // 设置自销毁
        // GameManager.Instance.DelayExecute.Add(projectile.DestroySelf, projectile.destroySecends);
    }

	
    public void DestroySelf(){
        if(gameObject.activeInHierarchy == true){
            // 设置速度为零
            move.Stop();
            gameObject.SetActive(false);
        }
    }

	void OnTriggerEnter2D(Collider2D col2d){
        
        // 如果碰撞的是砖块
		if(col2d.tag == "Brick"){
                        
            // 通过计算向左，向上偏移量得出起点位置
            Vector3 v3Origin = transform.position - transform.right*castLeftShift + transform.up * castUpShift;

            // 起点加上向右距离得出终点
            Vector3 v3End = v3Origin + transform.right * boxCastDistance;
            Debug.DrawLine(v3Origin, v3End, Color.blue, 30.0f);
            //RaycastHit2D[] hits = Physics2D.LinecastAll(v3Origin, v3End, castLayer);

            
            // 从碰撞体 上方偏移castUpShift, 左起偏移castLeftShift, 画boxCastSize大小, boxCastDistance距离的BoxCastAll
            RaycastHit2D[] hits = Physics2D.BoxCastAll(v3Origin, boxCastSize, 0.0f, transform.right, boxCastDistance, castLayer);

            // 获取祖先节点
            Transform ancestor = col2d.transform.parent.parent;
            if(ancestor != null){
                // 检查节点位置Node 可行进改变
                PathRequestManager.Instance.CheckGridNodeChange(ancestor.position);
            }

            // 被碰撞的砖块都被消除
            foreach(RaycastHit2D hit in hits){
				if(hit.transform.tag == "Brick"){
                	Destroy(hit.collider.gameObject);
				}
            }

            // 销毁自己
            DestroySelf();

		}else if(col2d.tag == "Enemy" && isEnemyShot == false){
            // TODO:  如果多发炮弹击中时会不会有同时访问 Health的可能? 待测试
            Health enemyHealth = col2d.GetComponent<EnemyHealth>();
            // col2d.GetComponent<Health>().health -= damage;
            // if(col2d.GetComponent<Health>())
            // 减血
            enemyHealth.TakeDamage(damage);
            
            DestroySelf();

        }else if(col2d.tag == "Player" && isEnemyShot == true){
            Health playerHealth = col2d.GetComponent<PlayerHealth>();

            playerHealth.TakeDamage(damage);
           
            DestroySelf();

		}else if(col2d.tag == "Home"){
            GameManager.Instance.GameOver();
			// Destroy (col2d.gameObject);
            DestroySelf();

		}else if(col2d.tag == "ProjectilePlayer" || col2d.tag == "ProjectileEnemy"){
            DestroySelf();
		}else if(col2d.tag == "Boundary" || col2d.tag == "Stone"){
            DestroySelf();
        }
        
        //Destroy(gameObject);
        // if(bulletHealth <= 0){
        //     Destroy(gameObject);
        // }
	}

}
