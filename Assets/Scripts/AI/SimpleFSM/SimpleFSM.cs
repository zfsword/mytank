using UnityEngine;
using System.Collections;

/// <summary>
///   简单FSM类
/// </summary>
public class SimpleFSM : MonoBehaviour{
    /// <summary>
    ///   简单FSM状态
    /// </summary>
    public enum SFSMState{
        None,  
        Patrol,
        Chase,
        Attack,
		ChaseBase,
		AttackBase,
        //Collide,
        Dead
    }

	public Transform targetTransform;	// 目标 
	public Transform playerTransform;	/// Player 的位置 
	public Transform baseTransform;		/// 基地位置

    Vector2 destPos;   /// 下一个目标位

    public Vector3[] patrolPointList;  /// 巡逻点列表

    public SFSMState curState;  /// 当前状态
    public SFSMState previousState; /// 前一个状态
    
    [Tooltip("满足此范围开始追击")]
    public float chaseDistance; /// 追击的范围

    public float attackDistance;  /// 开始攻击的范围,直线距离
    // public float attackAxisDistance;  /// 轴方向,开始攻击距离
    
        
    private AStarMove aStarMove;  // A Star 移动控制

    private Weapon weapon;   // 武器控制
    private Health health;   // 血量控制
    
    private bool isDead;     // 是否死亡

    
    void Awake(){
        // playerTransform = GameObject.FindWithTag("Player").transform;
		// baseTransform = GameObject.FindWithTag("Home").transform;

        aStarMove = GetComponent<AStarMove>();
        //aStarMove = GetComponent<AStarMoveByForce>();
        weapon = GetComponent<Weapon>();
        health = GetComponent<Health>();

	}

    void OnEnable(){

        Initialize();
    }

    void Start(){
        playerTransform = GameManager.Instance.Player.transform;
        baseTransform = GameManager.Instance.Home.transform;

    }

    void Update(){
        FSMUpdate();
    }


    
    /// <summary>
    ///   初始化方法
    /// </summary>
    void Initialize(){
        
                
        // 初始巡逻状态
        curState = SFSMState.Patrol;

        // destPos = transform.position;
    }


    /// <summary>
    ///   FSMUpdate
    /// </summary>
    void FSMUpdate(){
		switch (curState) {
            case SFSMState.Patrol:
                UpdatePatroState ();
                break;
            case SFSMState.Chase:
                UpdateChaseState ();
                break;
            case SFSMState.Attack:
                UpdateAttackState ();
                break;
            case SFSMState.ChaseBase:
                UpdateChaseBaseState ();
                break;
            case SFSMState.AttackBase:
                UpdateAttackBaseState ();
                break;
            case SFSMState.Dead:
                UpdateDeadState();
                break;
        }
    }

    /// <summary>
    ///   随机查找下一个巡逻点
    /// </summary>
    /// <returns> 巡逻点 </returns>
    protected Vector2 FindNextPatrolPoint(){
        if(patrolPointList != null && patrolPointList.Length>0){

            // 在0与巡逻点列表长度之间产生随机数
            int rndIndex = Random.Range(0, patrolPointList.Length);
            Vector2 rndPostion = Vector2.zero;
            // 返回相应路径点
            return patrolPointList[rndIndex];
            // return patrolPointList[rndIndex].position;
        }
        return Vector2.zero;
    }

    /// <summary>
    ///   更新巡逻状态
    /// </summary> 
    protected void UpdatePatroState(){
        // 如果到达巡逻点
        if(aStarMove.CouldAStarMove == true){
            // 设置下一个巡逻点
            destPos = FindNextPatrolPoint();
            aStarMove.TryAStarMoveToTarget(destPos);
            // Debug.Log("UpdatePatrolState, begin move next PatrolPoint");

        // 如果与player的距离小于追击距离
		} else {

			// float distToPlayer = Vector2.Distance (transform.position, playerTransform.position);
			// float distToHome = Vector2.Distance (transform.position, baseTransform.position);
            float distToPlayer = Vector2.SqrMagnitude(transform.position - playerTransform.position);
            float distToHome = Vector2.SqrMagnitude(transform.position - baseTransform.position);
		
			if (distToPlayer < Mathf.Pow(chaseDistance, 2.0f) || distToHome < Mathf.Pow(chaseDistance, 2.0f)) {
                aStarMove.Stop ();
				if(distToHome < distToPlayer){
					targetTransform = baseTransform;
					// 进入寻找基地状态
				    curState = SFSMState.ChaseBase;
				}else{
					targetTransform = playerTransform;
					// 改为追击状态
					curState = SFSMState.Chase;
				}
			}
		}
    }


    /// <summary>
    ///   追击状
    /// </summary>
    protected void UpdateChaseState(){

        // 检查与玩家之间的距离
        //float dist = Vector2.Distance(transform.position, homeTransform.position);
        Vector2 dist = transform.position - targetTransform.position;
        float dis = Vector2.SqrMagnitude(dist);
            
        // 如果距离小于攻击距离
        if(dis <= Mathf.Pow(attackDistance, 2.0f)){
            // 转为攻击状态
            curState = SFSMState.Attack;

        // 如果距离大于追击距离
        }else if(dis >= Mathf.Pow(chaseDistance, 2.0f)){
            // 转为巡逻状态
            curState = SFSMState.Patrol;
          
        }else{  // 追击状态
            //aStarMove.Stop();
            if(aStarMove.CouldAStarMove == true){

                Debug.Log("UpdateChaseState, Stop parto begin chase");
                // 设置目标点为玩家位置
                destPos = targetTransform.position;
                // 试用aStar移动到目标点
                aStarMove.TryAStarMoveToTarget(destPos);
            }
        }
        
    }



    /// <summary>
    ///   攻击状态
    /// </summary>
    protected void UpdateAttackState(){
        
        Vector3 dist = transform.position - targetTransform.position;

        Attack(targetTransform, 0.32f);

        //  如果距离超出攻击距离范围
        if(Vector3.SqrMagnitude(dist) > Mathf.Pow(attackDistance, 2.0f)){
            // 当前状态 改为追击状态
            curState = SFSMState.Chase;
        }
    }


	/// <summary>
	/// Updates the state of the chase base.
	/// </summary>
	void UpdateChaseBaseState(){
		Vector2 dist = transform.position - baseTransform.position;
		float dis = Vector2.SqrMagnitude(dist);
		if (aStarMove.CouldAStarMove == true) {

			//移动到基地附近
			// aStarMove.TryMoveToNearestNeighbour (baseTransform.position);
			// aStarMove.TryMoveToNearestNeighbour (transform.position, baseTransform.position);
			aStarMove.TryMoveToNearestNeighbour (transform.position, baseTransform.position);
            curState = SFSMState.AttackBase;
		// } else if (dis <= Mathf.Pow (attackDistance, 2.0f)) {
		// 	// 如果距离小于攻击距离
		// 	// 转为攻击状态
		// 	curState = SFSMState.AttackBase;	
		// } else if (dis >= Mathf.Pow (chaseDistance, 2.0f)) {
		// 	// 转为巡逻状态
		// 	curState = SFSMState.Patrol;
		}
	}


	/// <summary>
	/// the state of the attack base.
	/// </summary>
	void UpdateAttackBaseState(){
        Attack(baseTransform, 0.32f);

		//  如果距离超出攻击距离范围
		//if(Vector3.SqrMagnitude(dist) > Mathf.Pow(attackDistance, 2.0f)){
			// 当前状态 改为追击状态
		//	curState = SFSMState.Chase;
		//}

		// 移动到基地附近，横轴或纵轴与基地相同
		// 判断障碍是否存在
		// 攻击

	}

    /// <summary>
    ///  根据目标位置, 目标x或y轴向距离攻击目标
    /// </summary>
    /// <param name="target">攻击目标</param>
    /// <param name="axisMinDistance">x或y方向满足此最小距离</param>
    void Attack(Transform target, float axisMinDistance){
        Vector3 dist = transform.position - target.position;
        // 如果x方向 小于一个物体单位
        if (Mathf.Abs(dist.x) < axisMinDistance) {
            
            // 如果 target 在上面
            if (target.position.y > transform.position.y) {
                    // 如果停止移动了,转向
                if (aStarMove.CouldAStarMove == true) {
                    // 转向上方
                    aStarMove.Turn (AStarMove.AxisDirection.PositiveY);
                }
            } else { // 在下面
                if (aStarMove.CouldAStarMove == true) {
                    // 转向下方
                    aStarMove.Turn (AStarMove.AxisDirection.NegativeY);
                }
            }
            weapon.Attack (true);
            // 如果在y轴方向距离小于 
        } else if(Mathf.Abs(dist.y) < axisMinDistance) {
            
            // 如果 target在右边
            if (target.position.x > transform.position.x) {
                // 如果停止移动了
                if (aStarMove.CouldAStarMove == true) {
                    aStarMove.Turn (AStarMove.AxisDirection.PositiveX);
                }
            } else {
                // 如果停止移动了
                if (aStarMove.CouldAStarMove == true) {
                    aStarMove.Turn (AStarMove.AxisDirection.NegativeX);
                }
            }
            weapon.Attack (true);
        }

    }


    // TODO:多个物体同时碰撞考虑
    // 是否处于碰撞中
    /*private bool isColliding = false;

    void UpdateCollisionState(){
        if(isColliding == true){
            // 躲避行为
           
            // 转回上一个状态
            curState = previousState;
        }else{
            // 还原grid相关位置可移动属性
        }
    }
    */


    
   
    /// <summary>
    ///   死亡状态
    /// </summary>
    protected void UpdateDeadState(){
            
    }
    
}
