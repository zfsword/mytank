using UnityEngine;
using System.Collections;

/// <summary>
///   网格中的节点
/// </summary> 
public class Node : IHeapItem<Node> {

	public bool walkable;	///	节点是否可行进
	public Vector3 worldPosition;	/// 节点在世界坐标系位置
	public Node parent;	 	/// 父节点

	public int gridX;	/// 在网格中的X坐标
	public int gridY;	/// 在网格中的Y坐标
	public int movementPenalty;	/// 移动权值

	public int gCost;	/// g值
	public int hCost;	/// h值

	int heapIndex;	// 堆索引

	public Node(bool _walkable, Vector3 _worldPos, int _gridX, int _gridY, int _penalty){
		walkable = _walkable;
		worldPosition = _worldPos;

		gridX = _gridX;
		gridY = _gridY;

		movementPenalty = _penalty;
	}

    // F值
	public int fCost{
		get{ 
			return gCost + hCost;
		}
	}

	// 在堆中的索引
	public int HeapIndex {
		get{
			return heapIndex;
		}
		set{
			heapIndex = value;
		}
	}

    /// <summary>
	///  比较节点F值,如果比较点值 大于当前节点返回-1;等于返回0;小于返回1
    /// </summary>
    /// <param name="nodeToCompare"></param>   
	public int CompareTo(Node nodeToCompare){
        // 比较F值                
		int compare = fCost.CompareTo(nodeToCompare.fCost);
        // 如果F值相同，比较H值
		if(compare == 0){
			compare = hCost.CompareTo(nodeToCompare.hCost);
		}
        
		return -compare;
	}
}
