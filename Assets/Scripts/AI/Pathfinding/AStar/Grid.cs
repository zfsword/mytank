using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/// <summary>
///   A * Path finding 网格类
/// </summary>
public class Grid : MonoBehaviour {
	
	public bool displayGridGizmos;   /// 是否显示Gizmos信息
	public LayerMask unwalkableMask;	/// 不可行进LayerMask
	public Vector2 gridWorldSize;	/// 网格在世界坐标系的大小
	public float nodeRadius;	/// 节点半径

	public TerrainType[] walkableRegions;	/// 可行区域类型
	LayerMask walkableMask;	/// 可行进LayerMask
	Dictionary<int, int> walkableRegionsDictionary = new Dictionary<int, int>();	/// 存储可行区域<layer, walkablePenalty>

	Node[,] grid; 	/// 网格二维数组
	float nodeDiameter;  /// 节点直径
	int gridSizeX, gridSizeY;  /// 网格x, y 大小

	public int GridSizeY {
		get {
			return gridSizeY;
		}
		set {
			gridSizeY = value;
		}
	}

	public int GridSizeX {
		get {
			return gridSizeX;
		}
		set {
			gridSizeX = value;
		}
	}
	
	void Awake(){
		nodeDiameter = nodeRadius * 2; // 节点直径
		gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);	// 四舍五入求出网格x方向大小
		gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);


		foreach(TerrainType region in walkableRegions){
			// 计算所有可行区域layerMask
			walkableMask.value += region.terrainMask.value;
			// 将可行区域<layer, penalty>加入字典
			walkableRegionsDictionary.Add((int)Mathf.Log(region.terrainMask.value, 2), region.terrainPenalty);
		}

		CreateGrid();
	}



    // 网格最大节点数
	public int MaxSize{
		get{
			return gridSizeX * gridSizeY;
		}
	}

	void CreateGrid(){
        // 根据X,Y方向节点数量初始化网格
		grid = new Node[gridSizeX, gridSizeY];
		
		//	网格中心点减去右、上方向格一半，得到左下点
		// Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x * 0.5f - Vector3.up * gridWorldSize.y * 0.5f;
		Vector3 worldBottomLeft = new Vector3(transform.position.x - gridWorldSize.x * 0.5f, transform.position.y - gridWorldSize.y * 0.5f);


        Vector3 worldPos;  // 节点在世界坐标系位置
        Collider2D col2d;  // 碰撞体
        bool walkable = true;   //  节点是否可行进
        int movementPenalty = 0;  // 移动权值
        
		for(int x = 0; x < gridSizeX; x++){
			for(int y = 0; y < gridSizeY; y++){
				// 从左下点开始得出节点坐标
				// Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.up * (y * nodeDiameter + nodeRadius);
				worldPos = new Vector3(worldBottomLeft.x + x * nodeDiameter + nodeRadius, worldBottomLeft.y + y * nodeDiameter + nodeRadius);

				// 如果在节点半径范围内有物体碰撞则设置walkable为false，否则为true
				// bool walkable = !(Physics.CheckSphere(worldPoint, nodeRadius, unwalkableMask));

                //  缩小半径 以免覆盖到相邻节点
				col2d = Physics2D.OverlapCircle(new Vector2(worldPos.x, worldPos.y), nodeRadius-0.02f, unwalkableMask);
				//col2d = Physics2D.OverlapCircle(new Vector2(worldPos.x, worldPos.y), nodeRadius*0.5f, unwalkableMask);
				// Collider2D col2d = Physics2D.OverlapArea(new Vector2(worldPoint.x - nodeRadius/2, worldPoint.y - nodeRadius/2), new Vector2(worldPoint.x + nodeRadius/2, worldPoint.y + nodeRadius/2), unwalkableMask);
				walkable = col2d == null ? true:false;
				movementPenalty = 0;

				if(walkable){
					// 延y轴从上到下建立射线
					Ray ray = new Ray(worldPos + Vector3.up * 50, Vector3.down);
					RaycastHit hit;
					// 如果100长度的射线,在所有可行进layermask投射成功
					if(Physics.Raycast(ray, out hit, 100, walkableMask.value)){
						// 尝试获取collider的layer所对应的 移动权值
						walkableRegionsDictionary.TryGetValue(hit.collider.gameObject.layer, out movementPenalty);
					}
				}

				grid[x, y] = new Node(walkable, worldPos, x, y, movementPenalty);
			}
		}
	}
	

	/// <summary>
	/// 重新检查并网格中每个格的可行进性
	/// </summary>
	public void RecheckGridWalkable(){
		for(int x = 0; x < gridSizeX; x++){
			for(int y = 0; y < gridSizeY; y++){
				CheckAndSetWalkable(grid[x, y]);
			}
		}
	}

    /// <summary>
    ///   检查并设置节点是否可行进
    /// </summary>
    /// <param name="node">要检查和设置的节点</param>
    public void CheckAndSetWalkable(Node node){
        //  缩小半径 以免覆盖到相邻节点
        Collider2D col = Physics2D.OverlapCircle(node.worldPosition, nodeRadius-0.02f, unwalkableMask);
        // 如果碰撞体为空, walkable = true, 否则 false;
        bool walkable = col == null ? true : false;
        
        node.walkable = walkable; 
    }


    /// <summary>
    ///   设置节点可行进
    /// </summary>
    /// <param name="ndoe">要改变的节点</param>
    /// <param name="walkable">是否可行进</param>
    public void SetWalkable(Node node, bool walkable){
        node.walkable = walkable;

    }

	/// <summary>
	/// Gets the node.
	/// </summary>
	/// <returns>The node.</returns>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	public Node GetNode(int x, int y){
		if (x >= 0 && x < gridSizeX && y >= 0 && y < gridSizeY) {
			return grid [x, y];
		}else{
			return null;
		}
	}

    /// <summary>
    ///   代表邻居的四个方向
    /// </summary>
    public enum Neighbour{
        PositiveX,
        NegativeX,
        NegativeY,
        PositiveY
    }

    /// <summary>
    ///   获取相邻节点
    /// </summary>
    public Node GetNeighbour(Node node, Neighbour direction){
        int checkX = node.gridX;
        int checkY = node.gridY;

        if(direction == Neighbour.NegativeX){
            checkX = node.gridX - 1;
        }else if(direction == Neighbour.PositiveX){
            checkX = node.gridX + 1;
        }else if(direction == Neighbour.PositiveY){
            checkY = node.gridY + 1;
        }else if(direction == Neighbour.NegativeY){
            checkY = node.gridY - 1;
        }

        
        // 如果被检查点 在网格中
        if(checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY){
            return grid[checkX, checkY];
        }else{
            return null;
        }

    }
    
    /// <summary>
	/// 获取邻居节点
    /// <param name="node"> 需要获取邻居的节点</param>
    /// <returns> 邻居节点List<Node> </returns>
    /// </summary>
	public List<Node> GetNeighbours(Node node){
		List<Node> neighbours = new List<Node>();

        
        // 不考虑斜方向行走，与节点相邻的上下左右四个节点
		for(int x = -1; x <= 1; x++){
			for(int y = -1; y <= 1; y++){
                if(x == -1 && y == -1) continue;  // 排除左下节点
                if(x == -1 && y == 1) continue;  // 排除左上节点
				if(x == 0 && y == 0) continue;  // 排除本中心节点
                if(x == 1 && y == 1) continue;  // 排除右上节点
                if(x == 1 && y == -1) continue; // 排除右下节点
				
				int checkX = node.gridX + x;
				int checkY = node.gridY + y;
				
				// 如果被检查点 在网格中
				if(checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY){
					neighbours.Add(grid[checkX, checkY]);
				}
			}
		}
	
            
		// 考虑斜方向行走， 与节点相邻的8个节点
		// for(int x = -1; x <= 1; x++){
		// 	for(int y = -1; y <= 1; y++){
		// 		// 排除本中心节点
		// 		if(x == 0 && y == 0) continue;
		// 		
		// 		int checkX = node.gridX + x;
		// 		int checkY = node.gridY + y;
		// 		
		// 		// 如果被检查点 在网格中
		// 		if(checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY){
		// 			neighbours.Add(grid[checkX, checkY]);
		// 		}
		// 	}
		// }
		
		return neighbours;
	}

    /// <summary>
	/// 根据世界坐标系坐标, 返回Grid中所在节点
    /// <param name="worldPosition"> 世界坐标系中的位置</param>
    /// <returns>在网格中的节点</returns>
    /// </summary>
	public Node NodeFromWorldPoint(Vector3 worldPosition){
		// float percentX = (worldPosition.x + gridWorldSize.x/2) / gridWorldSize.x;
		// float percentY = (worldPosition.y + gridWorldSize.y/2) / gridWorldSize.y;
		// percentX = Mathf.Clamp01(percentX);
		// percentY = Mathf.Clamp01(percentY);
		
		// int x= Mathf.RoundToInt((gridSizeX-1) * percentX);
		// int y= Mathf.RoundToInt((gridSizeY-1) * percentY);
		float percentX = worldPosition.x/gridWorldSize.x + 0.5f;
		float percentY = worldPosition.y/gridWorldSize.y + 0.5f;
		
		int x = Mathf.RoundToInt((gridSizeX-1) * Mathf.Clamp01(percentX));
		int y = Mathf.RoundToInt((gridSizeY-1) * Mathf.Clamp01(percentY));
		
		return grid[x, y];
	}
	
	void OnDrawGizmos(){
		//  绘制整个网格边框
		Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, gridWorldSize.y, 1));

		//  绘制节点
		if(grid != null && displayGridGizmos){
            float showNodeSize = nodeDiameter - 0.03f;
			foreach(Node n in grid){
                // 红色不可行,白色可行进
				Gizmos.color = (n.walkable) ? Color.white : Color.red;
                
                //绘制cube
				// Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter - 0.1f));
				Gizmos.DrawCube(n.worldPosition, Vector3.one * showNodeSize);
			}
		}
	}

    /// <summary>
    ///   Tile地图类型, LayerMask 对应移动权值
    /// </summary>
	[System.Serializable]
	public class TerrainType{
		public LayerMask terrainMask;   /// LayerMask
		public int terrainPenalty;    /// LayerMask 对应的移动权值
	}
}
