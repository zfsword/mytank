﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[RequireComponent(typeof(Grid))]
public class NeighbourFinder : Singleton<NeighbourFinder>{

	Grid grid;

	public override void Awake(){
		base.Awake();
		grid = GetComponent<Grid>();
	}


	/// <summary>
	/// Get the nearest walkable position from origin with same X or Y of target position.
	/// 获取与目标点X或Y相同且与原点最近的可行进点坐标
	/// </summary>
	/// <returns>The nearest walkable same X or Y position.</returns>
	/// <param name="originPosition">Origin position.</param>
	/// <param name="targetPosition">Target position.</param>
	// public Vector3? GetNearestWalkableSameXOrYPosition(Vector3 originPosition, Vector3 targetPosition){

	// 	Node temp = null;
	// 	Vector3? pos = null;
	// 	Vector3 dist = originPosition - targetPosition;

	// 	// 如果x 方向大于 y方向距离, 优先走y方向点 更近
	// 	if(Mathf.Abs(dist.x) > Mathf.Abs(dist.y)){
	// 		temp = grid.NodeFromWorldPoint (new Vector2 (originPosition.x, targetPosition.y));
	// 	}else{
	// 		temp = grid.NodeFromWorldPoint (new Vector2 (targetPosition.x, originPosition.y));
	// 	}

	// 	if(temp.walkable == true){
	// 		pos = temp.worldPosition;
	// 	}
		
	// 	return pos;
	// }

	public void StartFindSameXorYNeighbour(Vector3 origin, Vector3 worldPos){
    	StartCoroutine(FindNearestWalkableSameXOrYPosition(origin, worldPos));
    }

	// public void StartFindSameXorYNeighbour(Vector3 worldPos){
    // 	StartCoroutine(FindNearestWalkableSameXOrYPosition(worldPos));
    // }

	/// <summary>
	/// Gets the nearest walkable same X or Y position.
	/// 寻找目标点上下左右最近的可行进点
	/// </summary>
	/// <returns>The nearest walkable X or Y position.</returns>
	/// <param name="worldPos">World position.</param>
	/// <param name="callback">Callback.</param>
	IEnumerator FindNearestWalkableSameXOrYPosition(Vector3 origin, Vector3 worldPos){
		bool getSuccess = false;
		Vector3? walkablePos = null;
		Vector3[] walkablePosArray = new Vector3[4];

		Node node = grid.NodeFromWorldPoint (worldPos);
		Node[] temp = new Node[4];

		int checkNX = 0;	// 负 x 轴检查
		int checkPX = 0;	// 正 x 轴检查
		int checkNY = 0;	// 负 y 轴检查
		int checkPY = 0;	// 正 y 轴检查

		bool[] finishChecks = new bool[4];

		// 不能无限循环
		for(int i=1;;i++){

			checkNX = node.gridX - i;
			checkPX = node.gridX + i;
			checkNY = node.gridY - i; // TODO: 可以简化 如果不考虑基地下方
			checkPY = node.gridY + i;

			// 检查点 越界或者此方向已成功找到可行进点
			finishChecks[0] = checkNX < 0;
			// 没找到且没越界
			if(finishChecks[0] == false){
				// 获取node
				temp[0] = grid.GetNode(checkNX, node.gridY);
			}

			finishChecks[1] = checkPX >= grid.GridSizeX;
			if(finishChecks[1] == false){
				temp[1] = grid.GetNode(checkPX, node.gridY);
			}

			finishChecks[2] = checkNY < 0;
			if(finishChecks[2] == false){
				temp[2] = grid.GetNode(node.gridX, checkNY);
			}

			finishChecks[3] = checkPY >= grid.GridSizeY;
			if(finishChecks[3] == false){
				temp[3] = grid.GetNode(node.gridX, checkPY);
			}

			for(int j=0;j<4;j++){
				if(finishChecks[j] == false){
					if (temp[j] != null) {
						if (temp[j].walkable == true) {
							finishChecks[j] = true;
							walkablePosArray[j] = temp[j].worldPosition;
							// getSuccess = true;
							// break;
						}
					}
				}
			}

			// 四个方向 都无可检查
			if(checkNX < 0 && checkPX > grid.GridSizeX && checkNY < 0 && checkPY > grid.GridSizeY){
				// 出了边界
				walkablePos = null;
				getSuccess = false;
				break;
			}


			if(finishChecks[0] && finishChecks[1] && finishChecks[2] && finishChecks[3]){
				getSuccess = true;
				walkablePos = NearestPosition(origin, walkablePosArray);
				break;
			}

		}

		yield return null;

		NeighbourRequestManager.Instance.FinishedProcessingNeighbour(walkablePos, getSuccess);
	
	} 

	// 查找出posList当中距离origin最近点
	Vector3 NearestPosition(Vector3 origin, Vector3[] posList){
		if(posList != null && posList.Length > 0){

			int minIndex = 0;
			float tempDist = 0.0f;
			float minDist = Vector2.SqrMagnitude(origin - posList[0]);

			for(int i=1;i<posList.Length;i++){
				tempDist = Vector2.SqrMagnitude(origin - posList[i]);
				if(tempDist < minDist){
					minDist = tempDist;
					minIndex=i;
				}
			}

			return posList[minIndex];
		}

		return Vector3.zero;

	}

	// // 查找x轴正方向可行进节点
	// IEnumerator FindSamePositiveX(Vector3 worldPos){
		
	// 	bool getSuccess = false;
	// 	Vector3? walkablePos = null;

	// 	Node node = grid.NodeFromWorldPoint (worldPos);
	// 	Node temp = null;

	// 	int check = 0;	// 负 x 轴检查

	// 	for(int i=1;;i++){
	// 		check = node.gridX + i;
	// 		if(check < grid.GridSizeX){
	// 			temp = grid.GetNode(check, node.gridY);
	// 			if (temp != null) {
	// 				if (temp.walkable == true) {
	// 					walkablePos = temp.worldPosition;
	// 					getSuccess = true;
	// 					break;
	// 				}
	// 			}
	// 		}else{
	// 			getSuccess = false;
	// 			break;
	// 		}
	// 	}

	// 	yield return null;

	// 	NeighbourRequestManager.Instance.FinishedProcessingNeighbour(walkablePos, getSuccess);

	// }

	// IEnumerator FindSameNegativeX(Vector3 worldPos){
		
	// 	bool getSuccess = false;
	// 	Vector3? walkablePos = null;

	// 	Node node = grid.NodeFromWorldPoint (worldPos);
	// 	Node temp = null;

	// 	int check = 0;	// 负 x 轴检查

	// 	for(int i=1;;i++){
	// 		check = node.gridX - i;
	// 		if(check >= 0){
	// 			temp = grid.GetNode(check, node.gridY);
	// 			if (temp != null) {
	// 				if (temp.walkable == true) {
	// 					walkablePos = temp.worldPosition;
	// 					getSuccess = true;
	// 					break;
	// 				}
	// 			}
	// 		}else{
	// 			getSuccess = false;
	// 			break;
	// 		}
	// 	}


	// 	yield return null;

	// 	NeighbourRequestManager.Instance.FinishedProcessingNeighbour(walkablePos, getSuccess);

	// }

	// IEnumerator FindSameNegativeY(Vector3 worldPos){
		
	// 	bool getSuccess = false;
	// 	Vector3? walkablePos = null;

	// 	Node node = grid.NodeFromWorldPoint (worldPos);
	// 	Node temp = null;

	// 	int check = 0;	// 负 x 轴检查

	// 	for(int i=1;;i++){
	// 		check = node.gridY - i;
	// 		if(check >= 0){
	// 			temp = grid.GetNode(node.gridX, check);
	// 			if (temp != null) {
	// 				if (temp.walkable == true) {
	// 					walkablePos = temp.worldPosition;
	// 					getSuccess = true;
	// 					break;
	// 				}
	// 			}
	// 		}else{
	// 			getSuccess = false;
	// 			break;
	// 		}
	// 	}

	// 	yield return null;

	// 	NeighbourRequestManager.Instance.FinishedProcessingNeighbour(walkablePos, getSuccess);

	// }

	// IEnumerator FindSamePositiveY(Vector3 worldPos){
		
	// 	bool getSuccess = false;
	// 	Vector3? walkablePos = null;

	// 	Node node = grid.NodeFromWorldPoint (worldPos);
	// 	Node temp = null;

	// 	int check = 0;	// 负 x 轴检查

	// 	for(int i=1;;i++){
	// 		check = node.gridY + i;
	// 		if(check < grid.GridSizeY){
	// 			temp = grid.GetNode(node.gridX, check);
	// 			if (temp != null) {
	// 				if (temp.walkable == true) {
	// 					walkablePos = temp.worldPosition;
	// 					getSuccess = true;
	// 					break;
	// 				}
	// 			}
	// 		}else{
	// 			getSuccess = false;
	// 			break;
	// 		}
	// 	}	

	// 	yield return null;

	// 	NeighbourRequestManager.Instance.FinishedProcessingNeighbour(walkablePos, getSuccess);

	// }
	

	// void FindNearestWalkableSameXOrYPosition(Vector3 origin, Vector3 worldPos){
	// 	Vector3 dire = origin - worldPos;
		
		
	// 	if(dire.x > dire.y){
	// 		if(origin.x > worldPos.x){
	// 			StartCoroutine(FindSamePositiveX(worldPos));
	// 		}
	// 		else{
	// 			StartCoroutine(FindSameNegativeX(worldPos));
	// 		}
	// 	}else{
	// 		if(origin.y > worldPos.y){
	// 			StartCoroutine(FindSamePositiveY(worldPos));
	// 		}
	// 		else{
	// 			StartCoroutine(FindSameNegativeY(worldPos));
	// 		}
	// 	}
	 
	// }



	

    /// <summary>
    ///   获取目标位置在网格中相邻可移动节点位置数组
    /// </summary>
    // public Vector3[] GetWalkableNeighboursInGrid(Vector3 worldPos){
    //     // 获取世界坐标系点对应在网格中的节点
    //     Node node = grid.NodeFromWorldPoint(worldPos);
    //     List<Node> neighbourNodes = grid.GetNeighbours(node);

    //     List<Vector3> neighbours = new List<Vector3>();
    //     foreach(Node n in neighbourNodes){
    //         if(n.walkable == true){
    //             neighbours.Add(n.worldPosition);
    //         }
    //     }

    //     return neighbours.ToArray();
    // }

	/// <summary>
	///   获取目标位置节点在网格中对应方向的相邻节点
	/// </summary>
	/// <param name="worldPos">将要计算的世界坐标系点</param>
	/// <param name="neighbour">要获取的邻居方向</param>
	// public Vector3? GetWalkableNeighbourByDirection(Vector3 worldPos, Grid.Neighbour neighbour){
	// 	// 获取世界坐标系点对应在网格中的节点
	// 	Node node = grid.NodeFromWorldPoint(worldPos);
	// 	node = grid.GetNeighbour(node, neighbour);

	// 	Vector3? neiPos = null;
	// 	// 如果节点不空并且可行进,返回其世界坐标系位置
	// 	if(node != null && true == node.walkable){
	// 		neiPos = node.worldPosition;
	// 	}else{
	// 		// 否则返回原位置
	// 		//return Vector3.worldPos;
	// 	}

	// 	return neiPos;
	// }


    /// <summary>
    /// 相对方向 
    /// </summary>
    // public enum RelativePosition{Left, Right, Forward, Back}
    
    /// <summary>
    ///   获取物体相对当前朝向方向，对应前后左右的邻居位置
    /// <param name="tran"> 物体transform </param>
    /// <param name="direction"> 相对方向 </param>
    /// </summary>
    // public Vector3? GetWalkableRelativeNeighbourPosition(Transform tran, RelativePosition direction){
    //     Grid.Neighbour relativeNei = 0;
    //     // 如果方向向上
    //     if(tran.up == Vector3.up){
    //         //
    //         if(direction == RelativePosition.Left){
    //             relativeNei = Grid.Neighbour.NegativeX;
    //         }else if(direction == RelativePosition.Right){
    //             relativeNei = Grid.Neighbour.PositiveX;
    //         }else if(direction == RelativePosition.Forward){
    //             relativeNei = Grid.Neighbour.PositiveY;
    //         }else if(direction == RelativePosition.Back){
    //             relativeNei = Grid.Neighbour.NegativeY;
    //         }
    //     }else if(tran.up == Vector3.down){
    //         if(direction == RelativePosition.Left){
    //             relativeNei = Grid.Neighbour.PositiveX;
    //         }else if(direction == RelativePosition.Right){
    //             relativeNei = Grid.Neighbour.NegativeX;
    //         }else if(direction == RelativePosition.Forward){
    //             relativeNei = Grid.Neighbour.NegativeY;
    //         }else if(direction == RelativePosition.Back){
    //             relativeNei = Grid.Neighbour.PositiveY;
    //         }
    //     }else if(tran.up == Vector3.left){
    //         if(direction == RelativePosition.Left){
    //             relativeNei = Grid.Neighbour.NegativeY;
    //         }else if(direction == RelativePosition.Right){
    //             relativeNei = Grid.Neighbour.PositiveY;
    //         }else if(direction == RelativePosition.Forward){
    //             relativeNei = Grid.Neighbour.NegativeX;
    //         }else if(direction == RelativePosition.Back){
    //             relativeNei = Grid.Neighbour.PositiveX;
    //         }
            
    //     }else if(tran.up == Vector3.right){
    //         if(direction == RelativePosition.Left){
    //             relativeNei = Grid.Neighbour.PositiveY;
    //         }else if(direction == RelativePosition.Right){
    //             relativeNei = Grid.Neighbour.NegativeY;
    //         }else if(direction == RelativePosition.Forward){
    //             relativeNei = Grid.Neighbour.PositiveX;
    //         }else if(direction == RelativePosition.Back){
    //             relativeNei = Grid.Neighbour.NegativeX;
    //         }
    //     }else{
    //         // 如果方向 不在上下左右
    //         return new Vector3?();
    //     }
    //     return GetWalkableNeighbourByDirection(tran.position, relativeNei);
    // }
    
    
    
    /// <summary>
    ///   获取target相对transform.up 相对方向位置
    /// </summary>
    // public RelativePosition GetTargetRelativeDirection(Transform trans, Vector3 target){
    //     // 目标方向
    //     Vector3 targetDirection = target - trans.position;
    //     // 点乘,正前方值为1,正后方为-1,水平方向为0, 相对在前在后
    //     float dotUp = Vector3.Dot(trans.up, targetDirection);
    //     // 点乘,正右方值为1,正左方为-1,水平方向为0, 相对在左在右
    //     float dotRight = Vector3.Dot(trans.right, targetDirection);
        
    //     // 比较水平轴和垂直轴 点乘绝对值大小, 值更大的, 说明更偏向于那个轴系
    //     if(Mathf.Abs(dotRight) > Mathf.Abs(dotUp)){
    //         if(dotRight < 0){
    //             Debug.Log("dot right 小于零，在左面");
    //             return RelativePosition.Left;
    //         }else{
    //             Debug.Log("dot right 大于零，在右面");
    //             return RelativePosition.Right;
    //         }
    //     }else{
    //         if(dotUp < 0){
    //             Debug.Log("dot up 小于零，在后面");
    //             return RelativePosition.Back;
    //         }else{
    //             Debug.Log("dot up 大于零，在前面");
    //             return RelativePosition.Forward;
    //         }
    //     }
    // }
	
	
		
}
