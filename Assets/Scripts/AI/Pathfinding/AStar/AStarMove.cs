﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



/// <summary>
/// 通过Update,实现A Star Move
/// </summary>
public class AStarMove: MonoBehaviour {
    
    public enum AxisDirection{
        PositiveX,
        NegativeX,
        PositiveY,
        NegativeY
    }
    
    public float speed = 1.0f;  /// 速度
    

    // 是否可以调用 A Star 方法移动
    public bool CouldAStarMove{
        get{
            return isFollowPathFinished;  
        }
    }
    
    public bool isFollowPathFinished = true; /// 按A Star查找到的路径是否移动完成
    
    public bool isMoveToTargetFinished = true;
    public Vector3[] aStarPath;             /// 查找到的路径点数组
    public int targetIndex = 0;            /// 移动目标路径点索引
    
    //private Vector3 currentPos; /// 当前位置
    private Vector3 targetPos;  /// 目标位置
     

    void OnEnable()
    {
        Stop();
    }
         
    void OnDisable()
    {
        Stop();
    }
	

    public void TryMoveToNearestNeighbour(Vector3 origin, Vector3 targetPos){
        NeighbourRequestManager.Instance.RequestNeighbour(origin, targetPos, OnNeighbourFound);
    }


	void OnNeighbourFound(Vector3? neighbourPos, bool success){
		if(neighbourPos.HasValue && success){
			// 试用aStar移动到目标点
			TryAStarMoveToTarget (neighbourPos.Value);
		}
		
	}

    /// <summary>
    ///  尝试通过A Star寻路移动到目标点
    /// </summary>
    /// <param name="target"> 目的点 </param>
    public void TryAStarMoveToTarget(Vector3 target){
        
        // 如果前一次移动完成
        if(CouldAStarMove){
            // 请求查找路径
            PathRequestManager.Instance.RequestPath(transform.position, target, OnPathFound);
            // 设置移动未完成
            isFollowPathFinished = false;
        }
    }
    


   
    /// <summary>
    ///   停止移动
    /// </summary>
    public void Stop(){
        aStarPath = null;
        // 重置寻路路径索引
        targetIndex = 0;

        // 设置移动完成
        isMoveToTargetFinished = true;
        // 设置a Star 移动完成
        isFollowPathFinished = true;

       
    }


    /// <summary>
    /// 请求路径回调方法
    /// 如果路径查找成功,将根据查找到的路径点移动物体
    /// </summary>
    /// <param name="newPath"> 查找到的路径 <param>
    /// <param name="pathSuccessful"> 查找路径是否成功 <param>
    void OnPathFound(Vector3[] newPath, bool pathSuccessful){
        
        //  如果查找路径成功
        if(pathSuccessful){
            // 如果新路径不空(有可能新目标点就是现在位置,导致新路径为空)
            if(newPath != null && newPath.Length > 0){
                //  将查找到的路径赋予path
                aStarPath = newPath;
                
                targetIndex = 0;
                isFollowPathFinished = false;
            }else{    // 如果新路径为空
                isFollowPathFinished = true;
            }
        }else{
            // 如果查找失败
            isFollowPathFinished = true;
        }
        
    }

    void Update(){
        if(isFollowPathFinished == false){
            if(isMoveToTargetFinished == true){

                // 如果索引大于等于 路径数组长度,停止移动
                if(aStarPath != null){
                    if(targetIndex >= aStarPath.Length){
                        Debug.Log("FollowPath, target Index is " + targetIndex + " aStarPath length is " + aStarPath.Length);
                        // 按寻路路径走完
                        // TODO:发生碰撞检测
                        isFollowPathFinished = true;
                        // targetIndex = 0;
                    }else{
                        // 取出下一个路径点
                        // Vector3 currentWaypoint = aStarPath[targetIndex];
                        // 设置移动点
                        // SeparateAndEnqueuePoint(currentWaypoint.x, currentWaypoint.y);

                        targetPos = aStarPath[targetIndex];
                        // 路径点索引加 1
                        targetIndex++;
                        isMoveToTargetFinished = false;
                        TurnHV();
                    }
                }
            }else{
                MoveToTarget();
            }
            
        }
    }    

    void MoveToTarget(){
        if(transform.position == targetPos){ 
            isMoveToTargetFinished = true;
        }else{
            transform.position = Vector2.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
        }
    }


 
	// 水平或垂直方向转向
	private void TurnHV(){
        if(targetPos.x > transform.position.x){
            Turn(AxisDirection.PositiveX);
		}else if(targetPos.x < transform.position.x){
			Turn(AxisDirection.NegativeX);
		}else{	// 如果X方向不移动，则y方向转向
			if(targetPos.y > transform.position.y){
				Turn(AxisDirection.PositiveY);
			}else if(targetPos.y < transform.position.y){
				Turn(AxisDirection.NegativeY);
			}
		}
	}


    /// <summary>
	/// 旋转方向
    /// </summary>
	public void Turn(AxisDirection direction){
		if(direction == AxisDirection.PositiveX)
		{
			transform.up = Vector2.right;
		}else if(direction == AxisDirection.NegativeX){
			transform.up = -Vector2.right;
		}else if(direction == AxisDirection.PositiveY){
			transform.up = Vector2.up;
		}else if(direction == AxisDirection.NegativeY){
			transform.up = -Vector2.up;
		}
	}


    void OnDrawGizmos(){
        //  如果路径点不空
        if(aStarPath != null){
            //  随着物体的移动, targetIndex会增大
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, targetPos);
            Gizmos.DrawCube(targetPos, new Vector3(0.2f, 0.2f, 0.2f));
            
            Gizmos.color = Color.cyan;
            for(int i = 0; i < aStarPath.Length; i++){
                Gizmos.DrawCube(aStarPath[i], new Vector3(0.2f, 0.2f, 0.2f));
                
            }
            
            Gizmos.color = Color.blue;
            for(int i = targetIndex; i < aStarPath.Length; i++){
                // 在每一个路径点画方块
                Gizmos.DrawCube(aStarPath[i], new Vector3(0.1f, 0.1f, 0.1f));
                
                //  从物体到第一个点之间画线
                /*if(i == targetIndex){
                    Gizmos.DrawLine(transform.position, aStarPath[i]);
                }else if(i < aStarPath.Length - 1){
                    //  每个点之间画线
                    Gizmos.DrawLine(aStarPath[i], aStarPath[i+1]);
                }*/
                if(i<aStarPath.Length-1){
                    Gizmos.DrawLine(aStarPath[i], aStarPath[i+1]);
                }
            }
        }
    }
}
