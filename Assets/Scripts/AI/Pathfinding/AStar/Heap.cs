using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// 最小二叉堆  minHeap
/// </summary>
/// <typeparam name="T"> <typeparamref name="T"> 实现IHeapItem 接口 </typeparam>
public class Heap<T> where T : IHeapItem<T> {
	
	T[] items;	/// 存储所有节点的数组
	int currentItemCount;	/// 当前节点数大小
	
    /// <summary> 堆构造函数 </summary>
    /// <param name="maxHeapSize"> 构造的堆栈大小 </param>
	public Heap(int maxHeapSize) {
		items = new T[maxHeapSize];
	}

    /// <summary>
    ///   添加节点
    /// <param name="item"> 要加入的对象 <typeparamref name="T"/> </param>
    /// </summary>
	public void Add(T item) {
        // 节点的索引设置为堆节点数
		item.HeapIndex = currentItemCount;
        // 将节点加入到末尾
		items[currentItemCount] = item;

		// 排序
		SortUp(item);

        // 堆节点数加 1
		currentItemCount++;
	}

    /// <summary>
	/// 取出根节点
    /// </summary>
	public T RemoveFirst() {
		T firstItem = items[0];
		currentItemCount--;
		items[0] = items[currentItemCount];	// 将最后节点移动至根节点
		items[0].HeapIndex = 0;
		SortDown(items[0]);	// 排序
		return firstItem;
	}

    /// <summary>
	/// 更新节点
    /// </summary>
    /// <param name="item"> 要更新的节点</param>
	public void UpdateItem(T item) {
		SortUp(item);
	}

    /// <summary>
    ///   只读属性,堆节点数量
    /// </summary>
    /// <returns>int 节点数量</returns>
    /// <value> <paramref name="currentItemCount"/> </value>
	public int Count {
		get {
			return currentItemCount;
		}
	}


    /// <summary>
    ///   堆中是否包含某个 item
    /// </summary>
	public bool Contains(T item) {
		return Equals(items[item.HeapIndex], item);
	}

    /// <summary>
	///  按顺序将节点向下降
    /// </summary>
	void SortDown(T item) {
		while (true) {
			int childIndexLeft = item.HeapIndex * 2 + 1;	// 左子节点索引
			int childIndexRight = item.HeapIndex * 2 + 2;	// 右子节点索引
			int swapIndex = 0;	// 临时索引

			// 如果左子节点不空
			if (childIndexLeft < currentItemCount) {
				swapIndex = childIndexLeft;

				// 如果右子节点不空
				if (childIndexRight < currentItemCount) {
					// 如果左子节点大于右子节点
					if (items[childIndexLeft].CompareTo(items[childIndexRight]) < 0) {
						swapIndex = childIndexRight;
					}
				}

				//  将当前节点与较小的子节点交换
				if (item.CompareTo(items[swapIndex]) < 0) {
					Swap (item,items[swapIndex]);
				}
				else {
					return;
				}

			}
			else {
				return;
			}
		}
	}

    /// <summary>
	/// 按顺序将节点向上升	
    /// </summary>
	void SortUp(T item) {
		int parentIndex = (item.HeapIndex-1)/2;		// 父节点索引
		
		while (true) {
			T parentItem = items[parentIndex];
			//  如果节点小于父节点值
			if (item.CompareTo(parentItem) > 0) {
				Swap (item,parentItem);
			}
			else {
				break;
			}
			parentIndex = (item.HeapIndex-1)/2;
		}
	}

    /// <summary>
	///  交换两节点位置
    /// </summary>
	void Swap(T itemA, T itemB) {
        // 交换节点位置
		items[itemA.HeapIndex] = itemB;
		items[itemB.HeapIndex] = itemA;
        // 交换索引
		int itemAIndex = itemA.HeapIndex;
		itemA.HeapIndex = itemB.HeapIndex;
		itemB.HeapIndex = itemAIndex;
	}
}

/// <summary>
///   堆内对象接口
/// </summary>
public interface IHeapItem<T> : IComparable<T> {

    /// <summary> 堆内索引 </summary>
    int HeapIndex {
            get;
            set;
    }
}
