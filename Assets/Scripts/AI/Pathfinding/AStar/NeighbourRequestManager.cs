﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NeighbourRequestManager : Singleton<NeighbourRequestManager>{

	Queue<NeighbourRequest> requestQueue = new Queue<NeighbourRequest> (); // 查找队列
	NeighbourRequest currentRequest;  // 当前查询
	bool isProcessing; // 是否正在查询


	/// <summary>
	/// 根据pos 请求查找邻居节点
	/// </summary>
	/// <param name="pos">要查找的位置坐标</param>
	/// <param name="callback">查找完成后回调方法</param>
	public void RequestNeighbour(Vector3 origin, Vector3 pos, Action<Vector3?, bool> callback){
		NeighbourRequest newRequest = new NeighbourRequest (origin, pos, callback);
		// 新的请求加入队列
		requestQueue.Enqueue (newRequest);

		TryProcessNextFindNeighbour ();
	}

	// 尝试处理下一个请求
	void TryProcessNextFindNeighbour(){
		if(isProcessing == false && requestQueue.Count > 0){
			currentRequest = requestQueue.Dequeue ();
			isProcessing = true;
			NeighbourFinder.Instance.StartFindSameXorYNeighbour(currentRequest.origin, currentRequest.pos);
		}
	}


	public void FinishedProcessingNeighbour(Vector3? pos, bool success){
		currentRequest.callback (pos, success);
		isProcessing = false;
		TryProcessNextFindNeighbour ();
	}

	public void StopAllProcess(){
		NeighbourFinder.Instance.StopAllCoroutines();
		requestQueue.Clear();
		isProcessing = false;

	}


	struct NeighbourRequest{
		public Vector3 origin;
		public Vector3 pos;
		public Action<Vector3?, bool> callback;

		public NeighbourRequest(Vector3 _origin, Vector3 _pos, Action<Vector3?, bool> _callback){
			origin = _origin;
			pos = _pos;
			callback = _callback;
		}
	}

}
