using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System;


/// <summary>
///   路径查找类
/// </summary>
[RequireComponent(typeof(Grid))]
public class Pathfinding : Singleton<Pathfinding>{
 
    Grid grid;


    public override void Awake(){
        base.Awake();
        grid = GetComponent<Grid>();
    }


    /// <summary>
    ///   开始查找路径,使用Unity 特有的协程 StartCoroutine
    /// </summary>
    /// <param name="startPos"> 起始位置 </param>
    /// <param name="targetPos"> 目标位置 </param>
    public void StartFindPath(Vector3 startPos, Vector3 targetPos){
    	StartCoroutine(FindPath(startPos, targetPos));
    }

    // 协程(Coroutine)查找路径
    IEnumerator FindPath(Vector3 startPos, Vector3 targetPos){

        // 计时器
    	Stopwatch sw = new Stopwatch();
    	sw.Start();
        
    	Vector3[] waypoints = new Vector3[0];
    	bool pathSuccess = false;

	    // 将坐标转换为网格中的节点
        Node startNode = grid.NodeFromWorldPoint(startPos);
        Node targetNode = grid.NodeFromWorldPoint(targetPos);

        // 如果起始节点和目标节点不相同
        if(startNode.Equals(targetNode) == false){

        // 如果起始节点和目标节点都可行进
        if(startNode.walkable && targetNode.walkable){
        	// 初始化开启列表
	        Heap<Node> openSet = new Heap<Node>(grid.MaxSize);
	        HashSet<Node> closedSet = new HashSet<Node>();

	        // 将起始节点加入开启列表
	        openSet.Add(startNode);
				
			// 如果开启列表不空
	        while (openSet.Count > 0){

	            // 将最小节点从开启列表删除,并加入关闭列表
	            Node currentNode = openSet.RemoveFirst();
	            closedSet.Add(currentNode);

				// 如果当前节点等于目标节点,查找结束
	            if (currentNode == targetNode){
	            	sw.Stop();
	            	// print("Path found: " + sw.ElapsedMilliseconds + "ms");
	            	pathSuccess = true;
	                break;
	            }

	            // 查询当前节点的邻居节点
	            foreach (Node neighbour in grid.GetNeighbours(currentNode)) {
	            	// 如果节点不可行,或者已经在关闭列表中, 则继续
	                if (!neighbour.walkable || closedSet.Contains(neighbour)) {
	                    continue;
	                }

	                // 计算到邻居节点的新G值, 并加上权值
	                int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour) + neighbour.movementPenalty;
	                // 如果新G值小于邻居节点的当前G值, 或者邻居节点不在开启列表中
	                // if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour)) {
	                //     neighbour.gCost = newMovementCostToNeighbour;
	                //     neighbour.hCost = GetDistance(neighbour, targetNode);
	                //     neighbour.parent = currentNode;

	                //     if (!openSet.Contains(neighbour)){
	                //         openSet.Add(neighbour);
	                //     }else{
	                // 		openSet.UpdateItem(neighbour);
	                // 	}
	                // }

                    // 如果邻居节点不在开启列表中
	                if(openSet.Contains(neighbour) == false){
                        // 计算G值
	                	neighbour.gCost = newMovementCostToNeighbour;
                        // 计算H值
	                	neighbour.hCost = GetDistance(neighbour, targetNode);
                        // 设置父节点为当前节点
	                	neighbour.parent = currentNode;
                        // 将此节点加入开启列表
	                	openSet.Add(neighbour);
                    // 如果新g值小于邻居节点的当前g值
	                }else if(newMovementCostToNeighbour < neighbour.gCost){
                        // 设置新g值
	                	neighbour.gCost = newMovementCostToNeighbour;
                        // 设置父节点为当前节点
	                	neighbour.parent = currentNode;
                        // 开启列表更新邻居节点
	                	openSet.UpdateItem(neighbour);
	                }
	            }
	        }
	    }
        }

        yield return null;

        // 如果查找成功
        if (pathSuccess){
            // 追朔路径
        	waypoints = RetracePath(startNode,targetNode);
        }
        
        // Debug.Log("FindPath Before Finished Processing  Path");
        // 通知路径请求管理类，寻路成功
        PathRequestManager.Instance.FinishedProcessingPath(waypoints, pathSuccess);
    }

    /// <summary>
    /// 追溯路径
    /// </summary>
    /// <param name="startNode"> 起始节点 </param>
    /// <param name="endNode"> 末节点 </param>
    Vector3[] RetracePath(Node startNode, Node endNode) {
        List<Node> path = new List<Node>();
        Node currentNode = endNode;

        // 将节点反序加入path列表
        while (currentNode != startNode) {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }

        Vector3[] waypoints = SimplifyPath(path);
        // 反转顺序
        Array.Reverse(waypoints);
        return waypoints;
    }

    /// <summary>
    /// 将节点List转化为世界坐标系位置路径点数组,从而减少路径点数，并且得到世界坐标系路径点
    /// </summary>
    Vector3[] SimplifyPath(List<Node> path){
    	List<Vector3> waypoints = new List<Vector3>();
    	Vector2 directionOld = Vector2.zero;

    	for(int i = 1;i < path.Count; i++){
	    	// path中路径的grid都是相邻的, 所以directionNew 是8个方向中的一个
    		Vector2 directionNew = new Vector2(path[i-1].gridX - path[i].gridX, path[i-1].gridY - path[i].gridY);
    		// 如果方向不同, 则发生转向,将节点加入路径点
    		if (directionNew != directionOld){
                // 将前一个节点加入路径点, 避免两点与角落相交
                waypoints.Add(path[i-1].worldPosition);
                // 将节点的世界坐标加入路径点
    			// waypoints.Add(path[i].worldPosition);
    		}
    		directionOld = directionNew;
    	}

        // 增加开始的节点
        waypoints.Add(path[path.Count-1].worldPosition);
    	return waypoints.ToArray();

    }

    // 计算两节点之间的值
    int GetDistance(Node nodeA, Node nodeB) {
        int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int dstY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        // 如果X方向距离大于Y方向距离, y方向全部斜走
       // if (dstX > dstY){
       //     return 14*dstY + 10 * (dstX-dstY);
       // }else{
       // 	return 14*dstX + 10 * (dstY-dstX);
       // }

        // 不考虑斜方向行走
        return  10 * (dstY + dstX);
    }

}
