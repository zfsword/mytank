using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/// <summary>
///  路径查找管理
/// </summary>
public class PathRequestManager: Singleton<PathRequestManager>{

	Queue<PathRequest> pathRequestQueue = new Queue<PathRequest>();	// 路径查找请求队列
	PathRequest currentPathRequest;	// 当前查找请求

	bool isProcessingPath;	// 是否正在查找路径

    Grid grid;  // 网格对象

	public override void Awake(){
		base.Awake();
        grid = GetComponent<Grid>();
	}

 
    /// <summary>
	/// 接收获取路径需求并装入队列
    /// <param name="pathStart">起点</param>
    /// <param name="pathEnd">终点</param>
    /// <param name="callback">回调方法, 返回Vector3路径, 和路径处理成功值</param>
    /// callback 参数是 <paramref name="Action<Vector3[], bool>"/>委托
    /// </summary>
	public void RequestPath(Vector3 pathStart, Vector3 pathEnd, Action<Vector3[], bool> callback){
        // 生成新的 PathRequest
		PathRequest newRequest = new PathRequest(pathStart, pathEnd, callback);
        // 将request装入队列
		pathRequestQueue.Enqueue(newRequest);
        // 尝试处理查找下一条路径
		TryProcessNextFindPath();
	}

    /// <summary>
	/// 尝试获取下一条路径
    /// </summary>
	void TryProcessNextFindPath(){
        // 如果 没有正在处理的查找路径 并且 路径查找队列不空
		if(isProcessingPath == false && pathRequestQueue.Count > 0){
            // 从队列中取出需求
			currentPathRequest = pathRequestQueue.Dequeue();
            // 设置正在处理查找路径请求
			isProcessingPath = true;
            // 开始查找路径
			Pathfinding.Instance.StartFindPath(currentPathRequest.pathStart, currentPathRequest.pathEnd);
		}
	}

    /// <summary>
	/// 路径查找完成
    /// </summary>
	public void FinishedProcessingPath(Vector3[] path, bool success){
        // 调用 PathRequest 回调方法
		currentPathRequest.callback(path, success);
        // 没有处理查找路径
		isProcessingPath = false;
        // 尝试处理下一个路径请求
		TryProcessNextFindPath();
	}

	public void StopAllProcess(){
		Pathfinding.Instance.StopAllCoroutines();
		pathRequestQueue.Clear();
		isProcessingPath = false;
	}

	/// <summary>
	/// 检查所有节点的可行进性
	/// </summary>
	public void CheckAllNodeWalkable(){
		grid.RecheckGridWalkable();
	}

    /// <summary>
    ///   检查某点网格walkable是否改变
    /// </summary>
    /// <param name="worldPos">世界坐标系中的位置</param>
    public void CheckGridNodeChange(Vector2 worldPos){
        // 获取世界坐标系点对应在网格中的节点
        Node node = grid.NodeFromWorldPoint(worldPos);
        // Debug.Log("node is " + node.worldPosition + node.gridX + " " + node.gridY);
        // 检查并设置节点是否可行进
        grid.CheckAndSetWalkable(node);
    }



    /// <summary>
    ///   路径查找结构体
    /// </summary>
	struct PathRequest{
		public Vector3 pathStart;  /// 请求起点
		public Vector3 pathEnd;   /// 请求终点
		public Action<Vector3[], bool> callback;	// 回调方法

		public PathRequest(Vector3 _start, Vector3 _end, Action<Vector3[], bool> _callback){
			pathStart = _start;
			pathEnd = _end;
			callback = _callback;
		}
	}
}
