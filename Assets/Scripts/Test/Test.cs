﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {

	void OnEnable(){
		Debug.Log("Enable");
	}

	void Awake(){
		Debug.Log("Awake");
	}

	// Use this for initialization
	void Start () {
		Debug.Log("Start");
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.W)){
			if(gameObject.activeInHierarchy == true){
				gameObject.SetActive(false);
			}else{
				gameObject.SetActive(true);
			}
		}
	}
}
