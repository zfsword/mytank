﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JustTest : MonoBehaviour{
	AStarMove move;
	Weapon weapon;

	void Awake(){
		move = GetComponent<AStarMove> ();
		weapon = GetComponent<Weapon>();
	}

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Backspace)){
			LevelManager.Instance.NextLevel();
		}

		if(Input.GetMouseButtonDown(1)){
            Vector3 mousePos = Input.mousePosition;
            Debug.Log("mouse input pos is " + mousePos);
            mousePos.z = 0.0f;
            Vector3 mouWorldPos = Camera.main.ScreenToWorldPoint(mousePos);
            
            move.TryAStarMoveToTarget(mouWorldPos);
        }
        
		if(Input.GetKeyDown(KeyCode.F)){
			GameObject bullet = weapon.projectilePrefab;
			Instantiate(bullet, transform.position, transform.rotation);
			Move move = bullet.GetComponent<Move>();
            if(move != null){
                move.direction = transform.up; // 设置移动方向
				move.Resume();
            }

		}
	}
}
