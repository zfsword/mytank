﻿using UnityEngine;
using System.Collections;

/// <summary>
///   基本移动
/// </summary>
public class Move : MonoBehaviour {

    [Header("移动速度")]
    public float initSpeed = 1.0f;  /// 初始移动速度
	public Vector2 direction = Vector2.up;  /// 移动方向

    // 用于物理移动的向量
	private Vector2 movement;

	void Start () {
		movement = initSpeed * direction;
        GetComponent<Rigidbody2D>().velocity = movement;
	}
	
    /// <summary>
    ///   设置移动速度
    /// </summary>
    /// <param name="sp">速度大小 </param>
    public void SetSpeed(float sp){
		movement = sp * direction;
        GetComponent<Rigidbody2D>().velocity = movement;
        
    }

	// Update is called once per frame
	void Update () {
		// movement = new Vector2(speed.x * direction.x, speed.y * direction.y);
	}

	void FixedUpdate(){
		//rigidbody2D.velocity = movement;

	}

    /// <summary>
    ///   停止移动
    /// </summary>
    public void Stop(){
        SetSpeed(0.0f);
    }

    public void Resume(){
        SetSpeed(initSpeed);
    }
}
