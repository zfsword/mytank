using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


// Game States
// public enum GameState{INTRO, MAIN_MENU}

// public delegate void OnStateChangeHandler();


public class GameManager : Singleton<GameManager>{


    [Tooltip("敌人生成位置")]
    public Vector3[] spawnPositions;   /// 生成位置

    [Tooltip("敌人总量")]
    [SerializeField]
    private int enemyAmount;          /// 敌人总数量

    [Tooltip("敌人在场景中最大数量")]
    [SerializeField]
    private int enemyMaxNumOnScene;     /// 敌人在场景中最大数量

    public int enemyHasBeenKilled;  //已经被消灭的敌人数量
    public int enemyCurrentCountOnScene; /// 当前场景敌人数量

    [Tooltip("生成敌人开始延时")]
    public float startInterval;     /// 生成敌人开始间隔

    [Tooltip("单个敌人生成间隔")]
    public float spawnInterval;     /// 每个敌人生成间隔

    [Tooltip("每波敌人生成间隔")]
    public float waveInterval;      /// 每波敌人生成间隔

    [Tooltip("巡逻点列表")]
    public Vector3[] patrolList;     /// 巡逻点列表
    public bool doingSetup;     // 是否正在构建

    [SerializeField]
    private int playerLife = 2;
    public int PlayerCurrentLife;

    [SerializeField]
    private Vector3[] playerSpawnPoints;  //玩家生成位置
    
    private GameObject player;
    public GameObject Player{get{return player;}}

    private GameObject home;   // 玩家基地
    public GameObject Home{get{return home;}}

    [SerializeField]
    private Vector3 homePos;    // 基地位置

    // public event OnStateChangeHandler OnStateChange;
    // public GameState gameState{get; private set;}


    // public void SetGameState(GameState state){
    //     this.gameState = state;
    //     OnStateChange();
    // }

    
    public override void Awake(){
        base.Awake();
        PlayerCurrentLife = playerLife;

        SceneManager.sceneLoaded += OnSceneFinishedLoading;
        SceneManager.sceneUnloaded += OnSceneUnloaded;
    }

    void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneFinishedLoading;
        SceneManager.sceneUnloaded -= OnSceneUnloaded;
    }

    void OnSceneFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        // 如果是游戏关卡
        if(scene.buildIndex > LevelManager.Instance.MaxLevelIndex || scene.buildIndex < LevelManager.Instance.MinLevelIndex ){
        }else{
            InitGame();
        }
    }

    void OnSceneUnloaded(Scene scene){
        StopAllProcess();
    }

    private Timer delayExecuter = null;
    // 用来延时执行方法的任务队列
    public Timer DelayExecuter{
        get{
            if(delayExecuter == null){
                delayExecuter = gameObject.GetComponent<Timer>();
            }
            return delayExecuter;
        }
    }

 
    public void InitGame(){
        doingSetup = true;

        StopAllProcess();
        enemyHasBeenKilled = 0;
        enemyCurrentCountOnScene = 0;

        // 载入巡逻点
        LoadPatrolPointList();

        // 重新检测地图所有节点 可行进性
        PathRequestManager.Instance.CheckAllNodeWalkable();

        
        // 初始化 对象池
        ObjectPooler.Instance.InitialPool();

        player = ObjectPooler.Instance.GetObjectByTag("Player");
        if(player != null){
            // 初始化玩家
            ResetPlayerPosition();
        }else{Debug.Log("player empty");}

        //  初始化Home
        home = ObjectPooler.Instance.GetObjectByTag("Home");
        if(home != null){
            home.transform.position = homePos;
            home.SetActive(true);
        }else{
            Debug.Log("Home is empty");
        }

              // 生成敌人
        StartCoroutine("SpawnEnemy");

        doingSetup = false;
    }
    

    void LoadPatrolPointList(){
        GameObject[] gos = GameObject.FindGameObjectsWithTag("PatrolPoint");
        if(gos != null){
            patrolList = new Vector3[gos.Length];
            for(int i=0;i<gos.Length;i++){
                patrolList[i] = gos[i].transform.position;
            }
        }
    }

    void LoadEnemySpawnPoints(){

    }

    public void ResetPlayerPosition(){
        
        player.gameObject.SetActive(false);
        // 两秒后重置 玩家 位置 
        StartCoroutine(CoroutineUtils.DelaySeconds(()=>{
            player.transform.position = playerSpawnPoints[0];
            player.transform.rotation = Quaternion.identity;
            player.gameObject.SetActive(true);
        }, 2.0f));

        // Player p = Instantiate(player, playerSpawnPoints[0], Quaternion.identity);
    }

    void ResetScore(){
        ScoreManager.Instance.ResetScore();
    }


    
    /// <summary>
    ///   生成敌人
    /// </summary>
    IEnumerator SpawnEnemy(){
        int enemyGenerated = 0;
        while(true){
            // 生成之前等待
            yield return new WaitForSeconds(startInterval);

            // 如果当前敌人数量 小于 场景最大敌人数量 
            for(;enemyCurrentCountOnScene < enemyMaxNumOnScene; enemyCurrentCountOnScene++){
                // 随机x坐标
                Vector3 spawnPos = spawnPositions[Random.Range(0, spawnPositions.Length)];
                Quaternion spawnRotation = Quaternion.identity;

                // 随即生成 敌人
                // Enemy enemy = enemyPrefabs[Random.Range(0, enemyPrefabs.Length)];
                GameObject enemy = ObjectPooler.Instance.GetObjectByTag("Enemy");
                enemy.SetActive(true);
                enemy.transform.position = spawnPos;

                // 设置巡逻点
                enemy.GetComponent<SimpleFSM>().patrolPointList = patrolList;

                // Instantiate(enemy, spawnPos, spawnRotation);
                // 生成等待间隔
                yield return new WaitForSeconds(spawnInterval);

                enemyGenerated++;
                // 如果生成的敌人数量大于等于敌人数量总数,停止
                if(enemyGenerated >= enemyAmount){
                    yield break;
                }
            }

            /*if(enemyGenerated >= enemyCount){
                yield break;
            }*/

            // 每一波的等待
            // yield return new WaitForSeconds(waveInterval);
        }
    }

    void StopAllProcess(){
        StopCoroutine("SpawnEnemy");
        StopAllCoroutines();
        PathRequestManager.Instance.StopAllProcess();
        NeighbourRequestManager.Instance.StopAllProcess();
        ObjectPooler.Instance.ClearPool();
    }

    void ResetAll(){
        ResetScore();
        PlayerCurrentLife = playerLife;
        enemyHasBeenKilled = 0;
        enemyCurrentCountOnScene = 0;

    }


   
    public void GameOver(){
        ResetAll();

        // GameManager.Instance.DelayExecuter.Add(()=>SceneManager.LoadScene("GameOver"), levelStartDelay-0.5f);
        LevelManager.Instance.LoadGameOver();
    }

    public void OnEnemyDeath(){
        enemyCurrentCountOnScene--;
        enemyHasBeenKilled++;
        if(enemyHasBeenKilled == enemyAmount){
            LevelManager.Instance.NextLevel();
        }
    }
}
