﻿using UnityEngine;

public class Pause : MonoBehaviour 
{
    [SerializeField] private GameObject pausePanel;
	
    void Start()
    {
        pausePanel.SetActive(false);
    }

    void Update()
    {
        if(Input.GetKeyDown (KeyCode.Escape)) 
        {
            Debug.Log(" pause menu active "+pausePanel.activeSelf);
            Debug.Log(" pause menu active in hierarchy "+pausePanel.activeInHierarchy);
            if (false == pausePanel.activeInHierarchy) 
            {
                PauseGame();
            }else
            {
                ResumeGame();
            }
        } 
    }
	 
    public void PauseGame()
    {
        Time.timeScale = 0;
        pausePanel.SetActive(true);
        //Disable scripts that still work while timescale is set to 0
    } 

    public void ResumeGame()
    {
        Time.timeScale = 1;
        pausePanel.SetActive(false);
        //enable the scripts again
    }
}