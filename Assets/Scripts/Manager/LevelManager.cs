using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : Singleton<LevelManager>{

    // public int[] notLevelSceneIndexs;   // 非游戏关卡场景
    
    [SerializeField]
    private int minLevelIndex;
    public int MinLevelIndex{get{return minLevelIndex;}}

    [SerializeField]
    private int maxLevelIndex;
    public int MaxLevelIndex{get{return maxLevelIndex;}}

    private int level; // 当前关卡
    public int Level{
        get{return level;}
    }

    public void NextLevel(){
        if(level == maxLevelIndex){
            LoadMLC();
        }else{
            LoadLevel(level+1);
        }
    }


    public void LoadFirstLevel(){
        LoadLevel(minLevelIndex);
    }

     // 加载游戏关卡
    public void LoadLevel(int lev){
        // 将关卡限制在游戏关卡内, 不包含 开始界面和结束界面
        if(lev >= minLevelIndex && lev <= maxLevelIndex){
            level = lev;
            SceneManager.LoadScene(lev);
        }
    }

    public void RestartLevel(){
        LoadLevel(level);
    }

    public void LoadMainScene(){
        SceneManager.LoadScene("Main");
    }

    public void LoadGameOver(){
        SceneManager.LoadScene("GameOver");
    }

    // Misstion All Cleared
    void LoadMLC(){
        SceneManager.LoadScene("MissionsAllCleared");
    }
}
