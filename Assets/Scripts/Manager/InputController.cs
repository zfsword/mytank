﻿using UnityEngine;
using System;

public class InputController: Singleton<InputController>{

	public bool couldInput = true;
	public float vertical;
	public float horizontal;
	// public Vector2 mouseInput;

    public bool fire1;
    public bool fire2;

    public bool Escape;

	void Update () {
		if(couldInput == false) return;

		Escape = Input.GetKeyDown(KeyCode.Escape);
		vertical = Input.GetAxis("Vertical");
		horizontal = Input.GetAxis("Horizontal");
		// mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        fire1 = Input.GetButton("Fire1");
        fire2 = Input.GetButton("Fire2");

	}
}