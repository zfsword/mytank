using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManager : Singleton<ScoreManager>{
    public Text scoreText;       /// 得分GuiText
    public int score = 0;    /// 得分

	public override void Awake(){
		base.Awake();

		SceneManager.sceneLoaded += OnLevelFinishedLoading;
	}

	void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode){

		if(scene.buildIndex > LevelManager.Instance.MaxLevelIndex || scene.buildIndex < LevelManager.Instance.MinLevelIndex ){
            ResetScore();
        }
    }

    /// <summary>
    ///   增加分数
    /// </summary>
    /// <param name="addScore"> 增加的分数</param>
    public void AddScore(int addScore){
        score += addScore;
        UpdateScore();
    }

    void UpdateScore(){
        scoreText.text = " SCORE " + score;
    }

    public void ResetScore(){
        score = 0;
        UpdateScore();
    }

}
