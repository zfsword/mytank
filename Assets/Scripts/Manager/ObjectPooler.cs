﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct PoolItem{
// public class PoolItem{
	public GameObject prefab;  // 池中对象
	public int amount;	// 数量
	public bool couldExpand;	//是否可扩容

}


public class ObjectPooler : Singleton<ObjectPooler>{

	public List<GameObject> objectsList;  // 对象池
	// public List<PoolItem> itemsToPool;
	public PoolItem[] items;
	
	// public override void Awake(){
	// 	base.Awake();
	// }

	// void Start(){
	// 	InitialPool();
	// }

	public void InitialPool(){
		// GameObject objectContainer = new GameObject("PoolObjectsContainer");
		objectsList = new List<GameObject>();

		foreach (PoolItem item in items)
		{
			for(int i=0;i<item.amount;i++){
				GameObject obj = (GameObject)Instantiate(item.prefab);
				// obj.transform.parent = objectContainer.transform;
				objectsList.Add(obj);
				obj.SetActive(false);
			}
		}
	}

	//通过tag获取对象
	public GameObject GetObjectByTag(string tag){
		int startIndex = 0;

		// 在每种对象中
		foreach(PoolItem item in items){
			// 如果此类item 不符合需要, 索引指针增加其数目, 从而到达下一种item 起始
			if(item.prefab.tag != tag){
				startIndex += item.amount;
			}else{
				// 在此种item 索引范围内,如果对象未激活, 将其返回
				for(int j=startIndex;j<startIndex+item.amount;j++){
					if(objectsList != null && j<objectsList.Count){
						if(objectsList[j].activeInHierarchy == false){
							return objectsList[j];
						}
					}
				}
			}
		}

		return null;
	}

	// public GameObject GetObjectByTag(string tag){
	// 	for(int i=0;i<objectsList.Count;i++){
	// 		//如果对象处于非激活状态并且符合需要的tag
	// 		if(!objectsList[i].activeInHierarchy && objectsList[i].tag == tag){
	// 			return objectsList[i];
	// 		}
	// 	}

	// 	// 循环之后，池中没有可用对象
	// 	foreach(PoolItem item in items){
	// 		if(item.prefab.tag == tag){
	// 			// 如果可以扩展
	// 			if(item.couldExpand){
	// 				GameObject obj = (GameObject)Instantiate(item.prefab);
	// 				obj.SetActive(false);
	// 				objectsList.Add(obj);
	// 				item.amount++;
	// 				return obj;
	// 			}
	// 		}
	// 	}

	// 	return null;
	// }

	// 通过每种Item 数量 ，直接索引到次item 地址 从而加快查找速度
	// 可以自动扩展对象数量
	public GameObject GetObjectByName(string name){
		int startIndex = 0;

		// 在每种对象中
		for(int i=0;i<items.Length;i++){
			// 如果此类item 不符合需要, 索引指针增加其数目, 从而到达下一种item 起始
			if(items[i].prefab.name != name){
				startIndex += items[i].amount;
			}else{
				// 在此种item 索引范围内,如果对象未激活, 将其返回
				for(int j=startIndex;j<startIndex+items[i].amount;j++){
					if(objectsList[j].activeInHierarchy == false){
						return objectsList[j];
					}
				}

				if(items[i].couldExpand == true){
					GameObject obj = (GameObject)Instantiate(items[i].prefab);
					obj.SetActive(false);
					// objectsList.Add(obj);
					// 使得列表按对象Name有序
					objectsList.Insert(startIndex, obj);
					items[i].amount++;
					return obj;
				}
			}
		}

		return null;
	}

	
	public void ResetPool(){
		for(int i=0;i<objectsList.Count;i++){
			if(objectsList[i] != null){
				objectsList[i].SetActive(false);
			}
		}
	}

	public void ClearPool(){
		for(int i=0;i<objectsList.Count;i++){
			if(objectsList[i] != null){
				objectsList[i].SetActive(false);
				Destroy(objectsList[i]);
				objectsList[i]=null;
			}
		}
		objectsList.Clear();
	}
}
