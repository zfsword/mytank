﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Health : MonoBehaviour {


    [SerializeField]
    private int health = 2;          // 血量

    public int currentHealth;

    void OnEnable()
    {
        currentHealth = health;
    }

    public bool IsDead{
        get{return currentHealth <= 0;}
    }

    public void TakeDamage(int damage){
        currentHealth -= damage;
        if(IsDead){
            Die();
        }
    }

	public virtual void Die(){
        if(OnDeath != null){
            OnDeath();
        }
	}
    
    public event System.Action OnDeath;


}
