﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : Health{

	public override void Die(){
		base.Die();

        ScoreManager.Instance.AddScore(GetComponent<Enemy>().score);
		GameManager.Instance.OnEnemyDeath();

		gameObject.SetActive(false);
	}
}
