﻿using UnityEngine;
using System.Collections;

/// <summary>
///   Player 移动,发射炮弹组件
/// </summary>
[RequireComponent(typeof(Weapon))]
public class Player : MonoBehaviour {

	public float speed = 1.5f;  //  移动速度
	public Weapon weapon;  //  武器组件
	public float objectRadius = 0.16f;  // 物体半径

	private Vector2 movement;	//通过Input获取横纵移动信息 * 移动速度的2维向量

	Rigidbody2D rb;

	// float leftBorder;
	// float rightBorder;
	// float topBorder;
	// float bottomBorder;

    
	// Use this for initialization
	void Awake(){
		weapon = GetComponent<Weapon>();
		rb = GetComponent<Rigidbody2D>();
	}

	void Start (){
		// 计算摄像机边界
		// float dist = (transform.position - Camera.main.transform.position).z;
		// objectRadius = 0.16f;  // 物体半径
		// // leftBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x + objectRadius;
		// // rightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x - objectRadius;
		// // topBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).y + objectRadius;
		// // bottomBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, dist)).y - objectRadius;
		// Vector2 bottomCorner = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist));
		// Vector2 topCorner = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, dist));

		// leftBorder = bottomCorner.x + objectRadius;
		// rightBorder = topCorner.x - objectRadius;
		// topBorder = bottomCorner.y + objectRadius;
		// bottomBorder = topCorner.y - objectRadius;

	}
	
	// Update is called once per frame
	void Update () {

		float inputX = InputController.Instance.horizontal;

		// 转向，并限制只能横、竖向移动
		float inputY = 0.0f;

        //  如果 横向存在移动
		if(inputX != 0){
		    //  水平转向
			transform.up = inputX > 0 ? Vector2.right : -Vector2.right;
		}else{
			inputY = InputController.Instance.vertical;
            //  如果存在纵向移动
			if(inputY != 0){
         
                //  垂直转向
                transform.up = inputY > 0 ? Vector2.up : -Vector2.up;
            }
		}

		movement = new Vector2(inputX,inputY)*speed;


		bool shoot = InputController.Instance.fire1;

		if (shoot)
		{
			if(weapon != null)
			{
				weapon.Attack(false);
				// SoundEffectsHelper.Instance.MakePlayerShotSound();
			}
		}

		// 保持在摄像机范围内
		// transform.position = new Vector3(Mathf.Clamp(transform.position.x, leftBorder, rightBorder),
		// 								 Mathf.Clamp(transform.position.y, topBorder, bottomBorder),
		// 								 transform.position.z
		// 								);
	}

	void FixedUpdate(){
		rb.velocity = movement;
        // rigidbody2D.AddForce(movement, ForceMode2D.Impulse);
	}

    
    void OnCollisionEnter2D(Collision2D coll)
	{
       if(coll.gameObject.tag == "Enemy"){
		   Health health = coll.gameObject.GetComponent<Health>();
		   if(health != null){
			   health.Die();
		   }
        }
	}

}
